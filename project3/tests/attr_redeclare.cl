(* This test will make sure that attributes
are not redefined *)

class Main { main() : Object {0}; };

class DefAttribute {
	attr1 : Object;
};

class UseAttribute inherits DefAttribute {
	attr1 : Object;
};
class Main {
 b:B <- new B;
 main(): Object { b@A.meth1(5,b,b,b) }; 
};

class A {
    foo(a:Int, b: B, c:A, d:B) : Int {
       5
    };
  
};

class B inherits A {

    foo(a:Int, b: B, c:A, d:B) : Int {
       6
    };  

    meth1() : String {
    	"test"
    };
};




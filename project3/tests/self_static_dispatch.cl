class Main {
	main() : Object {0};
	moo() : String {
		"test"
	};
};

class A inherits Main {
	a : A <- new A;
	moo() : String {
		"fart"
	};
	cow() : Object {
		a@SELF_TYPE.moo()
	};
};
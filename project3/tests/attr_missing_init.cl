(* This test will make sure that attributes
are always available to the class *)

class Main { main() : Object {0}; };

class DefAttribute {
	attr1 : Object;
};

class UseAttribute {
	attr2 : Object <- attr1;
};
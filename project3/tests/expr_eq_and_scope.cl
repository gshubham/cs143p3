(* This will check type checking on '=' exprs and whether scope 
	is handled properly for attributes vs. formals *)

class Main { main() : Object {0}; };

class A {
	x : String;
	method1 (x) : Object { x = "test" };
};

(* This will check whether LUB rules are checked for dispatch to verify methods *)

class Main { main() : Object {0}; };

class Base {};
class A inherits Base {
	method1 () : Bool { true };
};
class B {
	method2 () : Object {
		let var1 : Base <- new A in var1.method1() 
	};
};
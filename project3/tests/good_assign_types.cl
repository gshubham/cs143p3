class Main { 
	x : Int;
	y : String;
	z : Foo;

	main() : Object { 
		{	x <- 2; x <- 3; y <- "string"; z <- new Foo;} 
	}; 
};

class A {
  method1(x:Int) : Int {x <- 1};
};
(* Checks self attribute invalid *)

class Main { main() : Bool { true }; };

class A {
  self:A <- new A;
  method1(x:Int) : Int {x <- 1};
};
(* Checks whether self cannot be named *)
class Main {
	main() : Int {0};
	self : Int <- 5;
};
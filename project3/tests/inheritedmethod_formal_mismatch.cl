class A {
	foo(x:Int, y:Int) : Int {0};
	y:Int;
};

class B inherits A {
	foo(x:Int, y:String) : Int {0};
	y:Int;
};
class Main {
	main() : Object {0};
};
(* This checks redefinition base classes *)
class IO {};

(* This checks inheriting undefined classes *)
class B inherits C {};

(* Inheriting uninheritable class *)
class D inherits Int {};

(* Redefining previous class *)
class B inherits E {};

class E inherits B {};
class Main {
	main () : Int {0};
};

class A {
	moo () : String {"moo"};
};

class B inherits A {
	moo () : String {"mooooo"};
	moo () : String {"mooooooooo"};
};
(* This test will make sure that attributes
are not redefined from base class *)

class Main { main() : Object {0}; };

class DefAttribute inherits IO {
	val : Object;
};
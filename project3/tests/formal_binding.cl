class Main {
	main () : Int {0};
};

class A {
	moo () : String {"moo"};
};

class B inherits A {
	x : Int;
	bar (x : Int) : String {"mooooo"};
};
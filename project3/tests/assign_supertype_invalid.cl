(* Testing whether inheritance of classes works *)

class Main {
    a : A;
    b : B;
    main() : Object { b <- new A };
};

class A {};
class B inherits A {};

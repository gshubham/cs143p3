class Main { main() : Int {0 }; };

class A {
  x:String;
  meth1(x:A) : A {x};
};

class B inherits A {
  meth1(x:A,y:A) : A {self};
};

(* Checks whether scoping results in invalid dispatch *)

class Main { main() : Int {0 }; };

class A {
  x:String;
  foo(x:Int) : Object {x.length()};
};
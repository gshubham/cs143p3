(* This will ensure that dispatch is managed properly *)
class Main { main() : Object {0}; };

class A {
	method1() : Object {
		(new B).method2(self, true)
	};
};

class B inherits A {
	method2(b : B, x : Bool) : String { "test" };
};
class Main {
	i : Int;
	b : Bool;
	x : SELF_TYPE;
	foo() : SELF_TYPE { x };
	main() : Object { x };
};

class B inherits Main {
	y : SELF_TYPE;
	g(b : Object) : Object { 
		case b of 
			b: Int => i;
			b: Bool => b;
			b: Object => true;
		esac
	};
};
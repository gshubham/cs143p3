class Main { 
	main() : Int {0};
};

class Count {
	i : Int <- 0;
	inc () : SELF_TYPE {
		{
			i <- i + 1; 
			self;
		}
	};
};

class A inherits Count {
	inc () : SELF_TYPE {
		{
			i <- i + 2; 
			self;
		}
	};
};
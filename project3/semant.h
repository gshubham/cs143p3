#ifndef SEMANT_H_
#define SEMANT_H_

#include <assert.h>
#include <iostream>  
#include "cool-tree.h"
#include "stringtab.h"
#include "symtab.h"
#include "list.h"
#include <string>
#include <set>
#include <map>
#include <vector>
#include <algorithm>

#define TRUE 1
#define FALSE 0

typedef ClassTable *ClassTableP;

// This is a structure that may be used to contain the semantic
// information such as the inheritance graph.  You may use it or not as
// you like: it is only here to provide a container for the supplied
// methods.

class ClassTable {
private:
  int semant_errors;
  std::set<Class_> errorNodes;
  std::set<Class_> visitedNodes;
  Class_ curr_class;
  void DumpInheritanceMap();
  void install_basic_classes();
  bool checkFormalTypes(Feature one, Feature two, Class_ curr, Feature feature);
  bool checkForInheritance(Class_ curr, Features features, Feature currFeature);
  bool checkMultipleMethodDefinitions(Class_ curr, std::set<Feature>& currDefinitions, Feature currFeature);
  ostream& error_stream;
  void cycleRecurse(Class_ currNode, std::set<Class_>& visited);
  void markErrorNodes(Class_ errorNode);

public:
  std::map<Symbol, Class_> symToClass;
  std::map<Class_, std::set<Feature> > methodTable;
  std::map<Class_, std::set<Class_> > inheritanceGraph;
  SymbolTable<Symbol, Symbol> *symTab;
  ClassTable(Classes);
  int errors() { return semant_errors; }
  bool checkForCycles();
  void setCurrentClass(Class_ curr);
  Class_ getCurrentClass();
  ostream& semant_error();
  ostream& semant_error(Class_ c);
  void initializeEnvironment();
  void test();
  void buildMethodTable();
  void checkTypesAndScopes();
  void buildMethodTableRecurse(Class_ curr, Class_ parent);
  bool isSubtype(Symbol child, Symbol parent);
  Symbol lub(std::vector<Symbol>& types);
  ostream& semant_error(Symbol filename, tree_node *t);
};


#endif


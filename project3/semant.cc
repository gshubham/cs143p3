#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include "semant.h"
#include "utilities.h"

extern int semant_debug;
extern char *curr_filename;

//////////////////////////////////////////////////////////////////////
//
// Symbols
//
// For convenience, a large number of symbols are predefined here.
// These symbols include the primitive type and method names, as well
// as fixed names used by the runtime system.
//
//////////////////////////////////////////////////////////////////////
static Symbol 
    arg,
    arg2,
    Bool,
    concat,
    cool_abort,
    copy,
    Int,
    in_int,
    in_string,
    IO,
    length,
    Main,
    main_meth,
    No_class,
    No_type,
    Object,
    out_int,
    out_string,
    prim_slot,
    self,
    SELF_TYPE,
    Str,
    str_field,
    substr,
    type_name,
    val;
//
// Initializing the predefined symbols.
//
static void initialize_constants(void)
{
    arg         = idtable.add_string("arg");
    arg2        = idtable.add_string("arg2");
    Bool        = idtable.add_string("Bool");
    concat      = idtable.add_string("concat");
    cool_abort  = idtable.add_string("abort");
    copy        = idtable.add_string("copy");
    Int         = idtable.add_string("Int");
    in_int      = idtable.add_string("in_int");
    in_string   = idtable.add_string("in_string");
    IO          = idtable.add_string("IO");
    length      = idtable.add_string("length");
    Main        = idtable.add_string("Main");
    main_meth   = idtable.add_string("main");
    //   _no_class is a symbol that can't be the name of any 
    //   user-defined class.
    No_class    = idtable.add_string("_no_class");
    No_type     = idtable.add_string("_no_type");
    Object      = idtable.add_string("Object");
    out_int     = idtable.add_string("out_int");
    out_string  = idtable.add_string("out_string");
    prim_slot   = idtable.add_string("_prim_slot");
    self        = idtable.add_string("self");
    SELF_TYPE   = idtable.add_string("SELF_TYPE");
    Str         = idtable.add_string("String");
    str_field   = idtable.add_string("_str_field");
    substr      = idtable.add_string("substr");
    type_name   = idtable.add_string("type_name");
    val         = idtable.add_string("_val");
}


/* method : ClassTable constructor
 * -------------------------------
 * This method executes one pass to determine whether all basic inheritance/redefinition
 * rules are complied with. Then, in the second pass, builds an inheritance graph if possible
 * (i.e. everything inherits from a valid class).
 */
ClassTable::ClassTable(Classes classes) : semant_errors(0) , error_stream(cerr) {
    install_basic_classes();
    std::set<Symbol> classList;
    // TODO: Check if the graph is well-fromed b/c of IO/primitive craziness

    for(int i = classes->first(); classes->more(i); i = classes->next(i)) {
        Class_ curr = classes->nth(i);
        Symbol currName = curr->getName();
        Symbol currFilename = curr->get_filename();
        Symbol currParent = curr->getParent();
        // Cannot redefine basic classes
        if (currName == Bool || currName == Int || currName == Object || currName == Str || currName == IO || currName == SELF_TYPE) {
            semant_error(currFilename, curr);
            cerr << "Redefinition of basic class " << currName << endl;
        }

        // Cannot inherit from Int, Str, or Bool classes
        else if (currParent == Int || currParent == Str || currParent == Bool || currParent == SELF_TYPE) {
            semant_error(currFilename, curr);
            cerr << "Class " << currName << " cannot inherit from class " << currParent << endl;
        }
        else {
            // Cannot redefine existing class
            if(symToClass.count(currName) == 0) {
                    symToClass.insert(std::make_pair(currName, curr));
            }
            else {
                semant_error(currFilename, curr);
                cerr << "Class " << currName << " was previously defined." <<endl;
            }
        }
    }


    // Second pass to verify inheritance and build inheritence tree
    for(int i = classes->first(); classes->more(i); i = classes->next(i)) {
        Symbol parent = classes->nth(i)->getParent();
        // Object and IO are standard inherited classes
        // Check for Int/Str/Bool inheritence already passed, skip redundancy
        if(parent != Object && parent != Int && parent != Str && parent != Bool && parent != IO
            && symToClass.count(parent) == 0) {
            semant_error(classes->nth(i)->get_filename(), classes->nth(i));
            cerr << "Class "<< classes->nth(i)->getName() << " inherits from an undefined class " << parent <<endl;
        }
        else {
            inheritanceGraph[symToClass[parent]].insert(classes->nth(i));
        }
    }
}

/* This helper function is used for debugging the inheritance graph.
 * It dumps all classes and the set of subclasses.
 */
void ClassTable::DumpInheritanceMap(){
	for(std::map<Class_, std::set<Class_> >::iterator it_map = inheritanceGraph.begin(); it_map != inheritanceGraph.end(); it_map++){
		//cout << (it_map->first)->getName() << " : ";
		for(std::set<Class_>::iterator it_set = (it_map->second).begin(); it_set != (it_map->second).end(); it_set++){
			cout << (*it_set)->getName() << ", ";	
		}
		cout << endl;
	}
}

/* If a class has been recognized as part of a cycle, this goes through
 * and marks all of its children nodes as errorneous 
 * via a depth-first traversal.
 */
void ClassTable::markErrorNodes(Class_ errorNode) {
    if(errorNodes.find(errorNode) != errorNodes.end())
        return;
    errorNodes.insert(errorNode);
    semant_error(errorNode->get_filename(), errorNode);
    cerr << "Class " << errorNode->getName() << ", or an ancestor of " << errorNode->getName()
            <<", is involved in an inheritance cycle." << endl;
    for(std::set<Class_>::iterator it = inheritanceGraph[errorNode].begin(); it != inheritanceGraph[errorNode].end(); it++){
        // depth-first recursion
        markErrorNodes(*it);
    }
}


/* The recursion test follows a depth-first traversal of the
 * tree. If it notices that it is currently at a node that has
 * already been visited, then we have found a cycle and all 
 * child nodes are marked as errors.
 * 
 * Because of how the algorithm traverses the tree, the first time
 * we revisit a node will be the parent node of all error nodes. 
 *
*/
void ClassTable::cycleRecurse(Class_ currNode, std::set<Class_>& visited) {
    if (visitedNodes.find(currNode) != visitedNodes.end())
        return;
    if (errorNodes.find(currNode) != errorNodes.end())
        return;
    if (visited.find(currNode) != visited.end()){
        markErrorNodes(currNode);
        return;
    }


    for(std::set<Class_>::iterator it = inheritanceGraph[currNode].begin(); it != inheritanceGraph[currNode].end(); it++){
        visited.insert(currNode);
        cycleRecurse(*it, visited);
        visited.erase(currNode);
    }

}


/* This is the wrapper function that calls a recursive function to visit
 * all the connected components of the inheritance graph.  
 */
bool ClassTable::checkForCycles() {
    std::set<Class_> visited;
    for(std::map<Class_, std::set<Class_> >::iterator it = inheritanceGraph.begin(); it != inheritanceGraph.end(); it++){
    	cycleRecurse(it->first, visited);
    }

    // If there exist error nodes, then there was a cycle found.
    return errorNodes.size() == 0 ? false : true;
}



void ClassTable::install_basic_classes() {

    // The tree package uses these globals to annotate the classes built below.
   // curr_lineno  = 0;
    Symbol filename = stringtable.add_string("<basic class>");
    
    // The following demonstrates how to create dummy parse trees to
    // refer to basic Cool classes.  There's no need for method
    // bodies -- these are already built into the runtime system.
    
    // IMPORTANT: The results of the following expressions are
    // stored in local variables.  You will want to do something
    // with those variables at the end of this method to make this
    // code meaningful.

    // 
    // The Object class has no parent class. Its methods are
    //        abort() : Object    aborts the program
    //        type_name() : Str   returns a string representation of class name
    //        copy() : SELF_TYPE  returns a copy of the object
    //
    // There is no need for method bodies in the basic classes---these
    // are already built in to the runtime system.

    Class_ Object_class =
    class_(Object, 
           No_class,
           append_Features(
                   append_Features(
                           single_Features(method(cool_abort, nil_Formals(), Object, no_expr())),
                           single_Features(method(type_name, nil_Formals(), Str, no_expr()))),
                   single_Features(method(copy, nil_Formals(), SELF_TYPE, no_expr()))),
           filename);

    // 
    // The IO class inherits from Object. Its methods are
    //        out_string(Str) : SELF_TYPE       writes a string to the output
    //        out_int(Int) : SELF_TYPE            "    an int    "  "     "
    //        in_string() : Str                 reads a string from the input
    //        in_int() : Int                      "   an int     "  "     "
    //
    Class_ IO_class = 
    class_(IO, 
           Object,
           append_Features(
                   append_Features(
                           append_Features(
                                   single_Features(method(out_string, single_Formals(formal(arg, Str)),
                                              SELF_TYPE, no_expr())),
                                   single_Features(method(out_int, single_Formals(formal(arg, Int)),
                                              SELF_TYPE, no_expr()))),
                           single_Features(method(in_string, nil_Formals(), Str, no_expr()))),
                   single_Features(method(in_int, nil_Formals(), Int, no_expr()))),
           filename);  

    //
    // The Int class has no methods and only a single attribute, the
    // "val" for the integer. 
    //
    Class_ Int_class =
    class_(Int, 
           Object,
           single_Features(attr(val, prim_slot, no_expr())),
           filename);

    //
    // Bool also has only the "val" slot.
    //
    Class_ Bool_class =
    class_(Bool, Object, single_Features(attr(val, prim_slot, no_expr())),filename);

    //
    // The class Str has a number of slots and operations:
    //       val                                  the length of the string
    //       str_field                            the string itself
    //       length() : Int                       returns length of the string
    //       concat(arg: Str) : Str               performs string concatenation
    //       substr(arg: Int, arg2: Int): Str     substring selection
    //       
    Class_ Str_class =
    class_(Str, 
           Object,
           append_Features(
                   append_Features(
                           append_Features(
                                   append_Features(
                                           single_Features(attr(val, Int, no_expr())),
                                           single_Features(attr(str_field, prim_slot, no_expr()))),
                                   single_Features(method(length, nil_Formals(), Int, no_expr()))),
                           single_Features(method(concat, 
                                      single_Formals(formal(arg, Str)),
                                      Str, 
                                      no_expr()))),
                   single_Features(method(substr, 
                              append_Formals(single_Formals(formal(arg, Int)), 
                                     single_Formals(formal(arg2, Int))),
                              Str, 
                              no_expr()))),
           filename);

    // We add all the basic classes to our inheritance graph
    // in order to generate the complete method/inheritance tables
    symToClass.insert(std::make_pair(Object, Object_class));
    symToClass.insert(std::make_pair(IO, IO_class));
    inheritanceGraph[symToClass[Object]].insert(IO_class);
    symToClass.insert(std::make_pair(Int, Int_class));
    inheritanceGraph[symToClass[Object]].insert(Int_class);
    symToClass.insert(std::make_pair(Bool, Bool_class));
    inheritanceGraph[symToClass[Object]].insert(Bool_class);
    symToClass.insert(std::make_pair(Str, Str_class));
    inheritanceGraph[symToClass[Object]].insert(Str_class);
    symToClass.insert(std::make_pair(No_class, class_(No_class, No_class, nil_Features(), filename)));

}

////////////////////////////////////////////////////////////////////
//
// semant_error is an overloaded function for reporting errors
// during semantic analysis.  There are three versions:
//
//    ostream& ClassTable::semant_error()                
//
//    ostream& ClassTable::semant_error(Class_ c)
//       print line number and filename for `c'
//
//    ostream& ClassTable::semant_error(Symbol filename, tree_node *t)  
//       print a line number and filename
//
///////////////////////////////////////////////////////////////////

ostream& ClassTable::semant_error(Class_ c)
{                                                             
    return semant_error(c->get_filename(),c);
}    

ostream& ClassTable::semant_error(Symbol filename, tree_node *t)
{
    error_stream << filename << ":" << t->get_line_number() << ": ";
    return semant_error();
}

ostream& ClassTable::semant_error()                  
{                                                 
    semant_errors++;                            
    return error_stream;
} 


/* This is the iterate function for a class Node. It starts a new scope 
 * and adds all the attributes of the class within this scope. It then recurisvely calls
 * the iterator on all the features within this class (performing the recurisve descent)
 * and all calls the iterator of all the children classes of the current node in the inheritance
 * graph. This design helps keep track of inherited attributes as they are always in scope.
 * The function also performs basic redefinition checks and reports errors for the same.
*/
Symbol class__class::iterateOverFeatures(ClassTable* env) {
    // check to make sure doesn't inherit SELF_TYPE
    env->symTab->enterscope();
    if(env->getCurrentClass()->getName() != Object && env->getCurrentClass()->getName() != IO && env->getCurrentClass()->getName() != Str 
        && env->getCurrentClass()->getName() != Int && env->getCurrentClass()->getName() != Bool) {
        for(int i = features->first(); features->more(i); i = features->next(i)) {
            if(features->nth(i)->isMethod() == false) {
                if(env->symTab->lookup(features->nth(i)->getName()) != NULL) {
                    env->semant_error((env->getCurrentClass())->get_filename(), this);
                    cerr << "Redefinition of attribute " << features->nth(i)->getName()<<"."<<endl;
                }
                else {
                if(features->nth(i)->getName() == self) {
                    env->semant_error((env->getCurrentClass())->get_filename(), this);
                    cerr << "'self' cannot be the name of an attribute"<<endl;
                }
                else {
                     Symbol name = features->nth(i)->getName();
                     Symbol *type = new Symbol;
                     *type = features->nth(i)->getType();
                     env->symTab->addid(features->nth(i)->getName(), type);
                 }
                }
            }
        }
        for(int i = features->first(); features->more(i); i = features->next(i)) {
            features->nth(i)->iterate(env);
        }
    }
    Class_ curr = env->getCurrentClass();

    // All children will inherit the attributes of the current class.
    for(std::set<Class_>::iterator it = env->inheritanceGraph[curr].begin(); it != env->inheritanceGraph[curr].end(); it++) {
        env->setCurrentClass(*it);
        (*it)->iterateOverFeatures(env);
    }
    env->symTab->exitscope();
    return Object;
}

/* This is the iterate function for a method Node. It defines a new local scope and recurisvely calls
 * the iterator on all the formals within this class (performing the recurisve descent)
 * and all calls the iterator of expression. It then performs the conformance requirement of the return type of the expression.
*/
Symbol method_class::iterate(ClassTable* env) {
    env->symTab->enterscope();
    for(int i = formals->first(); formals->more(i); i = formals->next(i)) {
        formals->nth(i)->iterate(env);
    }
    Symbol exprType = expr->iterate(env);
    if(!env->isSubtype(exprType, return_type)) {
         env->semant_error((env->getCurrentClass())->get_filename(), this);
         cerr << "Inferred return type "<<exprType<< " of method "<<name<<" does not conform to declared return type "<<return_type<<"."<<endl;
    }
    env->symTab->exitscope();
    return return_type;
}

/* This is the iterate function for an attribute Node. It evaluates the initializer expression type and
 * checks the conformance requirement of the same. If not satisfied, it throws an error.
 */
Symbol attr_class::iterate(ClassTable* env) {
    Symbol exprType = init->iterate(env);
    if(!env->isSubtype(exprType, type_decl)) {
        env->semant_error((env->getCurrentClass())->get_filename(), this);
        cerr << "Inferred initialization type "<<exprType<< " of attribute "<<name<<" does not conform to declared static type "<<type_decl<<"."<<endl;
        return Object;
    }

    return exprType;
}

/* This is the iterate function for a formal Node. It first performs checks on redefinitions of formal
 * within the scope, name = 'self' and so on. Finally, it stores the formal in the symbol table and returns.
 */
Symbol formal_class::iterate(ClassTable* env) {

    if(env->symTab->probe(name) != NULL) {
        env->semant_error(env->getCurrentClass()->get_filename(), this);
        cerr << "Redefinition of formal " << name<<"."<<endl;
        return Object;
    }
    if(type_decl == SELF_TYPE) {
        env->semant_error(env->getCurrentClass()->get_filename(), this);
        cerr << "Declaration of formal with type as SELF_TYPE" <<endl;
        return Object;
    }
    if(name == self) {
        env->semant_error(env->getCurrentClass()->get_filename(), this);
        cerr << "'self' cannot be the name of a formal parameter." <<endl;
        return Object;
    }
    Symbol* typeSym = new Symbol;
    *typeSym = type_decl;
    env->symTab->addid(name, typeSym);
    return type_decl;
}

/* This is the iterate function for a case branch Node. It adds the name of the 
 * identifier to the current case scope and evaluates the type of the exrpession. The function
 * returns the type of the expression.
 */
Symbol branch_class::iterate(ClassTable* env) {
    Symbol* typeSym = new Symbol;
    *typeSym = type_decl;
    env->symTab->addid(name, typeSym);
    Symbol exprType = expr->iterate(env);
    return exprType;
}

/* This is the iterate function for an assign Node. It evaluates the initializer expression type and
 * checks the conformance requirement of the same. If not satisfied, it throws an error. It also stores the type
 * of the expression.
 */
Symbol assign_class::iterate(ClassTable* env) {
    if(env->symTab->lookup(name) == NULL) {
        env->semant_error(env->getCurrentClass()->get_filename(), this);
        cerr << "The identifier " << name << " is not declared in the current scope."<<endl;
        type = Object;
        return Object;
    }
    Symbol nameType = *(env->symTab->lookup(name));
    Symbol exprType = expr->iterate(env);
    if(!env->isSubtype(exprType, nameType)) {
        env->semant_error(env->getCurrentClass()->get_filename(), this);
        cerr << "The type of identifier" << name << " does not conform to the expression type while performing assignment." << endl;
        type = Object;
        return Object;
    }
    type = exprType;
    return type;
}

/* This is the iterate function for a static dispatch Node.
 */
Symbol static_dispatch_class::iterate(ClassTable* env) {
    // 1. Check whether the expression is a subtype of the class
    if(semant_debug){
        cout << "Static Dispatch: Checking dispatch on method " << name << endl;
    }
    Symbol exprType = expr->iterate(env);
    // a. Checks dispatch on SELF_TYPE
    // b. Determine static class type if possible
    if(exprType == SELF_TYPE){
        env->semant_error(env->getCurrentClass()->get_filename(), this);
        cerr << "Static dispatch to " << exprType << endl;
        type = Object;
        return type;        
    }
    std::map<Symbol, Class_>::iterator it = (env->symToClass).find(type_name);
    if(it == (env->symToClass).end()){
        env->semant_error(env->getCurrentClass()->get_filename(), this);
        cerr << "Static dispatch to undefined class " << type_name << "." << endl;
        type = Object;
        return type;
    }
    Class_ dispatchClass = it->second;

    // b. Verify expression is subtype
    if(!env->isSubtype(exprType, dispatchClass->getName())){
        env->semant_error(env->getCurrentClass()->get_filename(), this);
        cerr << "Expression type " << exprType << " does not conform to declared static dispatch type "
            << dispatchClass->getName() << "." << endl;
        type = Object;
        return type;
    }


    // 2. Check whether the class actually has the method defined
    std::set<Feature> scopedFeatures = ((env->methodTable).find(dispatchClass))->second;
    Formals methodFormals = NULL;
    Symbol methodType = Object;

    for(std::set<Feature>::iterator it = scopedFeatures.begin(); it != scopedFeatures.end(); it++){
        if(semant_debug){
            cout << "   Checking feature " << (*it)->getName() << endl;
        }
        if((*it)->isMethod() && (*it)->getName() == name){
            methodFormals = (*it)->getFormals();
            methodType = (*it)->getType();
            break;
        }
    }
    if(!methodFormals){
        env->semant_error(env->getCurrentClass()->get_filename(), this);
        cerr << "Static dispatch to undefined method " << name << "." << endl;
        type = Object;
        return type;
    }

    // 3. Carry out dispatch as regular with these preset values
    std::vector<Symbol> argTypes;
    for(int i = actual->first(); actual->more(i); i = actual->next(i)){
        // Iterate over the expressions to figure out their types
        argTypes.push_back(actual->nth(i)->iterate(env));
    }

    if((int) argTypes.size() != (int) methodFormals->len()){
        env->semant_error(env->getCurrentClass()->get_filename(), this);
        cerr << "Method " << name << " called with wrong number of arguments." << endl;
        type = Object;
        return type;
    }

    if(semant_debug)
        cout << "   The size of the arguments array is: " << argTypes.size() << endl;
    bool allGood = true;
    int counter = 0;
    for(int i = methodFormals->first(); methodFormals->more(i); i = methodFormals->next(i)){
        if(semant_debug){
            cout << "   Round " << counter << " checking formal: " << methodFormals->nth(i)->getName() << endl;
        }
        if(!env->isSubtype(argTypes[counter], methodFormals->nth(i)->getType())) {
            env->semant_error(env->getCurrentClass()->get_filename(), this);
            cerr << "In call of method " << name << ", type " << argTypes[counter] << " of parameter "
                << methodFormals->nth(i)->getName() << " does not conform to declared type " 
                << methodFormals->nth(i)->getType() << "." << endl;
            type = Object;
            allGood = false;
        }
        if(counter == (int) argTypes.size() - 1 && allGood){
            if(semant_debug) cout << "  Identified return type as: " << methodType << endl;
            type = methodType;
        }
        counter++;
    }

    if(methodFormals->len() == 0){
        type = methodType;
    }
    if(type == SELF_TYPE){
        return expr->iterate(env);
    }
    if(!allGood){
        if(semant_debug) cout << "  There were errors in the call, changing return type to Object" << endl;
        type = Object;
    }

    return type;
}

/* This is the iterate function for a dispatch Node.
 */
Symbol dispatch_class::iterate(ClassTable* env) {
    // 1. Figure out the class that this dispatch will be made to
    if(semant_debug){
        cout << "Dispatch: Checking dispatch on method " << name << endl;
    }
    Symbol exprType = expr->iterate(env);
    Class_ dispatchClass = exprType == SELF_TYPE ? env->getCurrentClass() : (env->symToClass).find(exprType)->second;
    // 2. Figure out if the method is available for the class

    if(semant_debug){
        cout << "   Found method " << name << " belonged to class " << dispatchClass->getName() << endl;
    }

    std::set<Feature> scopedFeatures = ((env->methodTable).find(dispatchClass))->second;
    Formals methodFormals = NULL;
    Symbol methodType = Object;

    for(std::set<Feature>::iterator it = scopedFeatures.begin(); it != scopedFeatures.end(); it++){
        if(semant_debug){
            cout << "   Checking feature " << (*it)->getName() << endl;
        }
        if((*it)->isMethod() && (*it)->getName() == name){
            methodFormals = (*it)->getFormals();
            methodType = (*it)->getType();
            break;
        }
    }
    if(!methodFormals){
        env->semant_error(env->getCurrentClass()->get_filename(), this);
        cerr << "Dispatch to undefined method " << name << endl;
        type = Object;
        return type;
    }

    std::vector<Symbol> argTypes;
    for(int i = actual->first(); actual->more(i); i = actual->next(i)){
        argTypes.push_back(actual->nth(i)->iterate(env));
    }

    if((int) argTypes.size() != (int) methodFormals->len()){
        env->semant_error(env->getCurrentClass()->get_filename(), this);
        cerr << "Method " << name << " called with wrong number of arguments." << endl;
        type = Object;
        return type;
    }

    bool allGood = true;
    int counter = 0;
    for(int i = methodFormals->first(); methodFormals->more(i); i = methodFormals->next(i)){
        if(!env->isSubtype(argTypes[counter], methodFormals->nth(i)->getType())) {
            env->semant_error(env->getCurrentClass()->get_filename(), this);
            cerr << "In call of method " << name << ", type " << argTypes[counter] << " of parameter "
                << methodFormals->nth(i)->getName() << " does not conform to declared type " 
                << methodFormals->nth(i)->getType() << "." << endl;
            type = Object;
            allGood = false;
        }
        if(counter == (int) argTypes.size() - 1 && allGood){
            if(semant_debug) cout << "  Identified return type as: " << methodType << endl;
            type = methodType;
        }
        counter++;
    }

    if(methodFormals->len() == 0)
        type = methodType;

    if(type == SELF_TYPE){
        if(semant_debug) cout << "  Noticed that method return was " << methodType << endl;
        type = exprType;
        return type;
    }

    if(!allGood) type = Object;
    return type;
}

/* This is the iterate function for a Conditional Node. It evaluates the type of the predicate
 * and throws error if not bool. It then sets the type of the expression.
 */
Symbol cond_class::iterate(ClassTable* env) {
   if(pred->iterate(env) != Bool) {
      env->semant_error(env->getCurrentClass()->get_filename(), this);
      cerr << "The predicate does not have type Bool." <<endl;
   }
   std::vector<Symbol> types;
   types.push_back(then_exp->iterate(env));
   types.push_back(else_exp->iterate(env));
   type = env->lub(types);
   return type;
}

/* This is the iterate function for a Loop Node. It evaluates the type of the predicate
 * and throws error if not bool. It then sets the type of the expression.
 */
Symbol loop_class::iterate(ClassTable* env) {
    Symbol predSymbol = pred->iterate(env);
    if(predSymbol != Bool){
        env->semant_error(env->getCurrentClass()->get_filename(), this);
        cerr << "Loop condition does not have type " << Bool << "." << endl;
    }
    body->iterate(env);

    type = Object;
    return type;
}

/* This is the iterate function for a Cases Node. It evaluates the expression, defines
 * a new scope for new definitions, and gets the types of all the branches. It then checks
 * for duplicate branches in the statement and finally sets the type of the expression.
 */
Symbol typcase_class::iterate(ClassTable* env) {
    expr->iterate(env);
    std::vector<Symbol> typesIdentifier;
    std::vector<Symbol> typesChecker;
    env->symTab->enterscope();
    for(int i = cases->first(); cases->more(i); i = cases->next(i)) {
        typesChecker.push_back(cases->nth(i)->iterate(env));
        typesIdentifier.push_back(cases->nth(i)->getDeclType());
    }
    env->symTab->exitscope();
    std::set<Symbol> currSet;
    for(int i = 0; i < (int) typesIdentifier.size(); i++) {
        if(currSet.find(typesIdentifier[i]) != currSet.end()) {
            env->semant_error(env->getCurrentClass()->get_filename(), this);
            cerr << "Duplicate branch " << typesIdentifier[i] << " in case statement." << endl;
            type = Object;
            return Object;
        }
        currSet.insert(typesIdentifier[i]);
    }
    type = env->lub(typesChecker);
    return type;
}

/* This is the iterate function for a Block Node. It evaluates the list of
 * expressions and sets the type of the node to be the type of the last
 * expresssion.
 */
Symbol block_class::iterate(ClassTable* env) {
    int listLength = body->len();
    for(int i = body->first(); body->more(i); i = body->next(i)) {
        if(i == listLength - 1) {
            type = body->nth(i)->iterate(env);
        }
        else {
            body->nth(i)->iterate(env);
        }
    }
    return type;
}

/* This is the iterate function for a Let Node.
 */
Symbol let_class::iterate(ClassTable* env) {

    if(identifier == self) {
        env->semant_error(env->getCurrentClass()->get_filename(), this);
        cerr << "Self encountered in a let binding"<<endl;
        type = Object;
        return type;
    }

    Symbol initType = init->iterate(env);
    if(initType != No_type){
        if(semant_debug){
            cout << "Type checking LET statement: " << endl;
            cout << "   Initializer type defined as: " << initType << endl;
        }
        if(!env->isSubtype(initType, type_decl)){
            env->semant_error(env->getCurrentClass()->get_filename(), this);
            cerr << "Inferred type " << initType << " of initialization of " 
                << identifier << " does not conform to identifier's declared type "
                << type_decl << endl;
            type = Object;
            if(semant_debug)
            return type;
        }
    }
    env->symTab->enterscope();
    Symbol* typeSym = new Symbol;
    *typeSym = type_decl;
    if(semant_debug){
        cout << "   Attempting to add " << identifier << " -> " << type_decl << endl;
    }
    env->symTab->addid(identifier, typeSym);
    type = body->iterate(env);
    env->symTab->exitscope();
    if(semant_debug) cout << "  Returning type of LET as: " << type << endl;
    return type;
}

/* This is the iterate function for a Plus Node. It evaluates the types of both
 * expressions and sets the type to be Int if both types are Int. Else, throws an error.
 */
Symbol plus_class::iterate(ClassTable* env) {
    if(e1->iterate(env) != Int) {
        env->semant_error(env->getCurrentClass()->get_filename(), this);
        cerr << "The first expression does not have type Int." << endl;
        type = Object;
        return Object;
     }
     if(e2->iterate(env) != Int) {
        env->semant_error(env->getCurrentClass()->get_filename(), this);
        cerr << "The second expression does not have type Int." << endl;
        type = Object;
        return Object;
     }
     type = Int;
     return type;
}

/* This is the iterate function for a Sub Node. It evaluates the types of both
 * expressions and sets the type to be Int if both types are Int. Else, throws an error.
 */
Symbol sub_class::iterate(ClassTable* env) {
     if(e1->iterate(env) != Int) {
        env->semant_error(env->getCurrentClass()->get_filename(), this);
        cerr << "The first expression does not have type Int." << endl;
        type = Object;
        return Object;
     }
     if(e2->iterate(env) != Int) {
        env->semant_error(env->getCurrentClass()->get_filename(), this);
        cerr << "The second expression does not have type Int." << endl;
        type = Object;
        return Object;
     }
     type = Int;
     return type;
}

/* This is the iterate function for a Multiply Node. It evaluates the types of both
 * expressions and sets the type to be Int if both types are Int. Else, throws an error.
 */
Symbol mul_class::iterate(ClassTable* env) {
     if(e1->iterate(env) != Int) {
        env->semant_error(env->getCurrentClass()->get_filename(), this);
        cerr << "The first expression does not have type Int." << endl;
        type = Object;
        return Object;
     }
     if(e2->iterate(env) != Int) {
        env->semant_error(env->getCurrentClass()->get_filename(), this);
        cerr << "The second expression does not have type Int." << endl;
        type = Object;
        return Object;
     }
     type = Int;
     return type;
}

/* This is the iterate function for a Divide Node. It evaluates the types of both
 * expressions and sets the type to be Int if both types are Int. Else, throws an error.
 */
Symbol divide_class::iterate(ClassTable* env) {
    if(e1->iterate(env) != Int) {
        env->semant_error(env->getCurrentClass()->get_filename(), this);
        cerr << "The first expression does not have type Int." << endl;
        type = Object;
        return Object;
     }
     if(e2->iterate(env) != Int) {
        env->semant_error(env->getCurrentClass()->get_filename(), this);
        cerr << "The second expression does not have type Int." << endl;
        type = Object;
        return Object;
     }
     type = Int;
     return type;
}

/* This is the iterate function for a Negation Node. It evaluates the types of the
 * expression and sets the type to be Int if the type is Int. Else, throws an error.
 */
Symbol neg_class::iterate(ClassTable* env) {
    if(e1->iterate(env) != Int) {
        env->semant_error(env->getCurrentClass()->get_filename(), this);
        cerr << "The expression does not have type Int." << endl;
        type = Object;
        return Object;
     }
     type = Int;
     return type;
}

/* This is the iterate function for a Lt Node. It evaluates the types of both
 * expressions and sets the type to be Bool if both types are Int. Else, throws an error.
 */
Symbol lt_class::iterate(ClassTable* env) {
     if(e1->iterate(env) != Int) {
        env->semant_error(env->getCurrentClass()->get_filename(), this);
        cerr << "The first expression does not have type Int." << endl;
        type = Object;
        return Object;
     }
     if(e2->iterate(env) != Int) {
        env->semant_error(env->getCurrentClass()->get_filename(), this);
        cerr << "The second expression does not have type Int." << endl;
        type = Object;
        return Object;
     }
     type = Bool;
     return type;
}

/* This is the iterate function for a Eq Node. It evaluates the types of both
 * expressions (Ints can only be compared with Ints, Bools with Bools, Strs with Strs)
 * . It then sets the type to be Bool if all the above conditions are satisfied. Else, throws an error.
 */
Symbol eq_class::iterate(ClassTable* env) {
    Symbol exprOneType = e1->iterate(env);
    Symbol exprTwoType = e2->iterate(env);
    if(exprOneType == Int || exprTwoType == Int) {
        if(!(exprOneType == Int && exprTwoType == Int)) {
            env->semant_error(env->getCurrentClass()->get_filename(), this);
            cerr << "Equaliy being tested where one expression has type Int and the other does not." << endl;
            type = Object;
            return Object;
        }
    }
    if(exprOneType == Bool || exprTwoType == Bool) {
        if(!(exprOneType == Bool && exprTwoType == Bool)) {
            env->semant_error(env->getCurrentClass()->get_filename(), this);
            cerr << "Equaliy being tested where one expression has type Bool and the other does not." << endl;
            type = Object;
            return Object;
        }
    }
    if(exprOneType == Str || exprTwoType == Str) {
        if(!(exprOneType == Str && exprTwoType == Str)) {
            env->semant_error(env->getCurrentClass()->get_filename(), this);
            cerr << "Equaliy being tested where one expression has type String and the other does not." << endl;
            type = Object;
            return Object;
        }
    }
    type = Bool;
    return type;
}

/* This is the iterate function for a Leq Node. It evaluates the types of both
 * expressions and sets the type to be Bool if both types are Int. Else, throws an error.
 */
Symbol leq_class::iterate(ClassTable* env) {
    if(e1->iterate(env) != Int) {
        env->semant_error(env->getCurrentClass()->get_filename(), this);
        cerr << "The first expression does not have type Int." << endl;
        type = Object;
        return Object;
     }
     if(e2->iterate(env) != Int) {
        env->semant_error(env->getCurrentClass()->get_filename(), this);
        cerr << "The second expression does not have type Int." << endl;
        type = Object;
        return Object;
     }
     type = Bool;
     return type;
}

/* This is the iterate function for a Comp Node. It evaluates the type of the
 * expression and sets the type to be Bool if the type is Bool. Else, throws an error.
 */
Symbol comp_class::iterate(ClassTable* env) {
     if(e1->iterate(env) != Bool) {
        env->semant_error(env->getCurrentClass()->get_filename(), this);
        cerr << "The negation expression does not have type Bool" << endl;
        type = Object;
        return Object;
     }
     type = Bool;
     return type;
}

/* This is the iterate function for a Int_Const Node. It sets the type of the node to Int.
 */
Symbol int_const_class::iterate(ClassTable* env) {
    type = Int;
    return type;
}

/* This is the iterate function for a Bool_Const Node. It sets the type of the node to Bool.
 */
Symbol bool_const_class::iterate(ClassTable* env) {
    type = Bool;
    return type;
}
/* This is the iterate function for a Str_Const Node. It sets the type of the node to Str.
 */
Symbol string_const_class::iterate(ClassTable* env) {
    type = Str;
    return type;
}
/* This is the iterate function for a New Node. It sets the type of the node to the type of the declared type_name.
 * Throws an error if the type is not declared.
 */
Symbol new__class::iterate(ClassTable* env) {
    if(type_name == SELF_TYPE) {
        type = SELF_TYPE;
        return  type;
    }
    if(env->symToClass.find(type_name) == env->symToClass.end()) {
        env->semant_error((env->getCurrentClass())->get_filename(), this);
        cerr << "'new' used with undefined class " << type_name <<"."<<endl;
        type = Object;
        return type;
    }
    type = type_name;
    return type;
 }

 /* This is the iterate function for a IsVoid Node. It sets the type of the node to Bool and evaluates the expression.
 */
Symbol isvoid_class::iterate(ClassTable* env) {
    e1->iterate(env);
    type = Bool;
    return type;
}

Symbol no_expr_class::iterate(ClassTable* env) {
    if(semant_debug){
        cout << "   Evaluating type of no_expr_class" << endl;
    }
    type = No_type;
    return type;
}

/* self - always has type SELF_TYPE
 * No_class, SELF_TYPE - returns current class (just to be sure)
 * Otherwise, checks whether the attribute is in scope and returns appropriate
 * value.
 */
Symbol object_class::iterate(ClassTable* env) {
    if(semant_debug) cout << "In object_class iterator" << endl;
    
    if(name == self) {
        if(semant_debug) cout << "  Returning SELF_TYPE" << endl;
        type = SELF_TYPE;
        return SELF_TYPE;
    }


    if(name == SELF_TYPE || name == No_type) {
        if(semant_debug) cout << "  Returning the converted SELF_TYPE" << endl;
        type = env->getCurrentClass()->getName();
        return type;
    }

    if(env->symTab->lookup(name)){
        type = *(env->symTab->lookup(name));
        return type;
    } 
    else {
        env->semant_error((env->getCurrentClass())->get_filename(), this);
        cerr << "Undeclared identifier " << name << endl;
        type = Object;
        return type;
    }
    return Object;
}

/* InitializeEnvironment : 
 * Initializes the symbol table to be used in the program
 */
void ClassTable::initializeEnvironment() {
    symTab = new SymbolTable<Symbol, Symbol>();
}

/* test: 
 * Provides a function to make calls to and debug member functions.
 */
void ClassTable::test() {
    cout << "Parent of Object class is: " << symToClass[Object]->getParent() << endl;
    std::string bool_type = symToClass[Object]->getParent() == No_class ? "true" : "false";
    cout << "_no_class == No_class? : " << bool_type << endl;
    bool_type = ClassTable::isSubtype(Object, Bool) ? "true" : "false";
    cout << "Is Object subtype of Bool? " << bool_type << endl;
    bool_type = ClassTable::isSubtype(Bool, Object) ? "true" : "false";
    cout << "Is Bool subtype of Object? " << bool_type << endl;    
    std::vector<Symbol> types;
    Symbol types_array [] = { Bool, Int, Str, IO };
    types.insert(types.begin(), types_array, types_array + 4);
    cout << "The LUB of Bool, Int, Str, and IO is: " << lub(types) << endl;
}

/* checkFormalTypes:
 * Takes in two features and checks if both have the same formal types. Returns true if they do, else returns false
 */
bool ClassTable::checkFormalTypes(Feature one, Feature two, Class_ curr, Feature feature) {
    Formals oneFormals = one->getFormals();
    Formals twoFormals = two->getFormals();
    for(int i = oneFormals->first(); oneFormals->more(i); i = oneFormals->next(i)) {
        if(oneFormals->nth(i)->getType() != twoFormals->nth(i)->getType()) {
            semant_error(curr->get_filename(), curr);
            cerr << "In redefined method " << feature->getName() << ", parameter type " << twoFormals->nth(i)->getType() <<" is different from original type " <<oneFormals->nth(i)->getType()<<"."<<endl;
            return false;
        }
    }
    return true;
}

/* checkForInheritance
 * Takes in a feature and a current class and checks whether the inherited method has a legal type signature (same as the parent).
 */
bool ClassTable::checkForInheritance(Class_ curr, Features features, Feature currFeature) {
    for(std::set<Feature>::iterator it = methodTable[curr].begin(); it != methodTable[curr].end(); it++) {
                if((*it)->getName() == currFeature->getName()) {
                    if((*it)->getType() != currFeature->getType()) {
                        semant_error(curr->get_filename(), currFeature);
                        cerr << "In redefined method " << currFeature->getName() << ", return type " << currFeature->getType() <<" is different from original return type " <<(*it)->getType()<<"."<<endl;
                        return false;
                    }
                    if((*it)->getFormals()->len() != currFeature->getFormals()->len()) {
                        semant_error(curr->get_filename(), currFeature);
                        cerr << "Incompatible number of formal parameters in redefined method " << currFeature->getName()<<"."<<endl;
                        return false;
                    } 
                    if(!checkFormalTypes(*it, currFeature, curr, currFeature)) {
                        return false;
                    }
                    return true;
                }
    }
    return true;
}
/* checkForInheritance
 * Takes in a list of features and checks whether the current feature is already defined (had multiple definitions within the same class). Returns
 * false if multiple declarations are found.
 */
bool ClassTable::checkMultipleMethodDefinitions(Class_ curr, std::set<Feature>& currDefinitions, Feature currFeature) {
    
    for(std::set<Feature>::iterator it = currDefinitions.begin(); it != currDefinitions.end(); it++) {
        if((*it)->getName() == currFeature->getName()) {
            semant_error(curr->get_filename(), currFeature);
            cerr << "Method " << currFeature->getName() << " is multiply defined."<<endl;
            return false;
        }
    }
    return true;
}
/* buildMethodTableRecurse:
 * The recurse function that takes in a curent and a parent class and builds the legal method list for the current class. It then calls itself
 * recursively onto current's children.
 */
void ClassTable::buildMethodTableRecurse(Class_ curr, Class_ parent) {
    if(semant_debug) cout<<"Looking at class " <<curr->getName()<<". Getting all its methods into the method table"<<endl;
    if(parent != NULL) {
        methodTable[curr] = methodTable[parent];
    }
    std::set<Feature> currDefinitions;
    Features features = curr->getFeatures();
    for(int i = features->first(); features->more(i); i = features->next(i)) {
        if(features->nth(i)->isMethod() == true) {
            if(checkForInheritance(curr, features, features->nth(i)) && checkMultipleMethodDefinitions(curr, currDefinitions, features->nth(i))) {
             if(semant_debug) cout<<"    Feature "<<features->nth(i)->getName()<<" of class "<<curr->getName()<<" is being added to the table"<<endl;
                currDefinitions.insert(features->nth(i));
            }
              
        } 
    }
    for(std::set<Feature>::iterator it = currDefinitions.begin(); it != currDefinitions.end(); it++)
        methodTable[curr].insert(*it);
       if(curr->getName() == Main) {
        bool flag = false;
        for(std::set<Feature>::iterator it = methodTable[curr].begin(); it != methodTable[curr].end(); it++) {
            if((*it)->getName() == main_meth) flag = true;
        }
        if(!flag) {
             semant_error(curr->get_filename(), curr);
             cerr << "Method main() is not defined in Class Main."<<endl;
        } 
    }
    for(std::set<Class_>::iterator it = inheritanceGraph[curr].begin(); it != inheritanceGraph[curr].end(); it++) {
        buildMethodTableRecurse(*it, curr);
    }
}

void ClassTable::buildMethodTable() {
    buildMethodTableRecurse(symToClass[Object], NULL);
}

/* isSubtype
 * Returns true if the child is a subtype of the parent. Else returns false.
 */
bool ClassTable::isSubtype(Symbol child, Symbol parent) {

    if(child == No_type) return true;
    if(parent == SELF_TYPE && child != SELF_TYPE) return false;
    if(child == SELF_TYPE) child = curr_class->getName();
    if(parent == SELF_TYPE) parent = curr_class->getName();
    if(semant_debug){
        cout << "   Attempting to check whether " << child << " is subtype of " << parent << endl;
    }
    Class_ currClassTest = symToClass[child];
    if(child == parent || child == No_type || parent == Object){ 
        if(semant_debug) cout << "  TRUE" << endl;
        return true; 
    }
    if(child == Object) {
        if(semant_debug) cout << "  FALSE" << endl;
        return false;
    }

    int counter = 1;
    while(currClassTest->getName() != No_class){
        if(semant_debug) cout << counter << ": " << currClassTest->getName() << "==" << parent << endl;
        if(currClassTest->getName() == parent) return true;
        currClassTest = symToClass[currClassTest->getParent()];
        if(semant_debug) counter++;
    }
    return false;
}

Symbol ClassTable::lub(std::vector<Symbol>& types) {
    if(types.empty()) return Object;
    Symbol base = types[0];
    if(base == SELF_TYPE) {
        bool all_selftype = true;
        for(unsigned int i = 0; i < types.size(); i++){
            if(types[i] != SELF_TYPE) all_selftype = false;
        }
        if (all_selftype){
            return SELF_TYPE;
        }
        else { 
            base = curr_class->getName();
        }
    }
    while(base != Object){
        bool all_passed = true;
        for(std::vector<Symbol>::iterator it = types.begin(); it != types.end(); it++){
            if(!isSubtype(*it, base)) all_passed = false;
        }
        if (all_passed) return base;
        base = symToClass[base]->getParent();
    }
    return Object;
}
void ClassTable::setCurrentClass(Class_ curr) {
    curr_class = curr;
}
Class_ ClassTable::getCurrentClass() {
    return curr_class;
}

void ClassTable::checkTypesAndScopes() {
    curr_class = symToClass[Object];
    if(symToClass.find(Main) == symToClass.end())  {
        cerr << "Class Main is not defined." <<endl;
        semant_errors++;
    }
    symToClass[Object]->iterateOverFeatures(this);
}


/*   This is the entry point to the semantic checker.

     Your checker should do the following two things:

     1) Check that the program is semantically correct
     2) Decorate the abstract syntax tree with type information
        by setting the `type' field in each Expression node.
        (see `tree.h')

     You are free to first do 1), make sure you catch all semantic
     errors. Part 2) can be done in a second stage, when you want
     to build mycoolc.
 */
void program_class::semant()
{
    initialize_constants();
    /* ClassTable constructor may do some semantic analysis */
    ClassTable *classtable = new ClassTable(classes);
    if (classtable->errors()) {
        cerr << "Compilation halted due to static semantic errors." << endl;
        exit(1);
    }
    if ( classtable->checkForCycles() ) {
        cerr << "Compilation halted due to static semantic errors." << endl;
        exit(1);
    }

    classtable->initializeEnvironment();

    classtable->buildMethodTable();

    classtable->checkTypesAndScopes();

    if (classtable->errors()) {
        cerr << "Compilation halted due to static semantic errors." << endl;
        exit(1);
    }

}
